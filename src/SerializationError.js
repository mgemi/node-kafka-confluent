function SerializationError(err, encoder, data) {
  this.name = 'SerializationError';
  this.stack = (new Error()).stack;

  let paths = []
  encoder.isValid(data, {errorHook: hook})
  this.message = 'Message format error - invalid fields: ' + paths.join(', ')

  function hook(path, value) {
      paths.push(path.join() );
  }
}
SerializationError.prototype = Object.create(Error.prototype);
SerializationError.prototype.constructor = SerializationError;


module.exports = SerializationError
