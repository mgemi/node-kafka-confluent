
const avro = require('avsc')
const Kafka = require('no-kafka')
const fs = require('fs')

const SchemaHelper = require('./SchemaHelper')
const SerializationError = require('./SerializationError')

module.exports = function(config){
    let _producer, _producerPromise

    return {
        producerOf: producerOf,
        init: init,
        SerializationError: SerializationError
    }

    function init(){
        console.log("Initializing Kafka Service")
        _producer = new Kafka.Producer(config)
        _producerPromise = _producer.init()
    }

    function producerOf(topicName, schemaString){
        if(!_producer) init()
        let schemaId
        const encoder = avro.parse(JSON.parse(schemaString))
        const validate = SchemaHelper.validate(config, topicName, schemaString)
            .then( registration => { schemaId = registration.id })

        return {
            schema: schemaString,
            send: function(key, data){
                return _producerPromise
                    .then( () => validate )
                    .then( () => {
                        const encodedData = encode(schemaId, encoder, data  )
                        return _producer.send({
                          topic: topicName,
                          message: {
                              key   : key,
                              value : encodedData
                          }
                      }, { batch: config.batch })
                    })
                    .catch(err => {
                        throw err;
                    })
            },
            sendBatch: function(events){
                return _producerPromise
                    .then( () => validate )
                    .then( () => events.map( e => {
                            return {
                              topic: topicName,
                              message: { key   : e.key,  value : encode(schemaId, encoder, e.value  ) }
                            }
                        })
                    )
                    .then( (payload) => _producer.send(payload, { batch: config.batch }) )
                    .catch(err => {
                        throw err;
                    })
            }
        }
    }

    function encode(schemaId, encoder, data){
        if(!schemaId) throw new Error("invalid schema id: "+schemaId)

        const magicByte = Buffer.from([0x00])
        const idBuffer = Buffer.alloc(4);
        idBuffer.writeUInt32BE(schemaId)
        try {
             const encodedAvroData = encoder.toBuffer(data)
             return Buffer.concat([magicByte, idBuffer, encodedAvroData])
        } catch(err){
            throw new SerializationError(err, encoder, data)
        }
    }
}
