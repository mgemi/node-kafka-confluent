const http = require('request')

const MAX_ATTEMPTS = 10
const NEXT_ATTEMPT_DELAY = 5 * 50

module.exports = {
    validate: validate
}


function validate(config, topic, schema, _attempts){


  return new Promise( (resolve, reject) => {
    function requestLoop(attempt, delay){
      const req = {
          uri: `${config.schemaRegistry}/subjects/${topic}-value/versions`,
          json: {   schema: schema }
      }
      http.post(req, (err, result) => {
        if(err || (result && result.statusCode !== 200 ) ){
          if(attempt > MAX_ATTEMPTS){
            console.error(`Abandoning attempt to register schema after ${attempt} tries (${topic})`)
            reject(err)
          }
          else {
            const nextDelay = delay * 2
            console.error(`Schema registry error with topic ${topic} - attempt ${attempt}/${MAX_ATTEMPTS}, trying again in ${nextDelay}ms`, err, result ? result.statusCode : null)
            setTimeout( () => requestLoop(attempt + 1, nextDelay), nextDelay)
          }
        }
        else resolve(result.body)
      })
    }

    requestLoop(1, 100)
  })
}
