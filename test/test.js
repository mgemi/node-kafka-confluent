const fs = require('fs')
const sinon = require('sinon')
const Kafka = require('no-kafka')
const KafkaFactory = require('../src/KafkaFactory')

describe('KafkaFactory',() => {

  var schemaString, kafka, producerStub
  before( () => {
    kafka = KafkaFactory({

    })
    schemaString = fs.readFileSync(__dirname + '/Heartbeat.avsc', 'utf-8')

    producerStub = sinon.stub(Kafka, 'Producer')
    producerStub.returns({
      init: () => Promise.resolve({})
    })

  })

  it('should track changed values',() =>{
    console.log("kafka", kafka)

    const producer = kafka.producerOf("my_topic", schemaString)

    return producer.send('foo', 'bar')
      .then( () => {
        console.log("OK")
      })
      .catch( err => {
        console.log("ERR", err)
      })
  })

})
